import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private firestore: AngularFirestore,private toastr: ToastrService) { }

  getMovies(): Observable<any>{

    return this.firestore.collection('films',ref =>ref.orderBy('title','asc')).snapshotChanges();
  }

  getWish(): Observable<any>{

    return this.firestore.collection('wish1',ref =>ref.orderBy('title','asc')).snapshotChanges();
  }

  eraseMovie(id:string): Promise<any>{
    return this.firestore.collection('films').doc(id).delete();

  }
  eraseWish(id:string): Promise<any>{
    return this.firestore.collection('wish1').doc(id).delete();

  }

  getToWish(id:string):Observable<any>{

    return this.firestore.collection('films').doc(id).snapshotChanges();
  }

  addToWish(id:string,data:any):Promise <any>{
    return this.firestore.collection('wish1').doc(id).set(data);
  }

  getToMovies(id:string):Observable<any>{
    return this.firestore.collection('wish1').doc(id).snapshotChanges();
   
          
    
  }

  addToMovies(id:string,data:any):Promise <any>{
    return this.firestore.collection('films').doc(id).set(data);
  }

  existInWish(id:string){
    
  }
}
