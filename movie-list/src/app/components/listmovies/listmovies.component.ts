import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { MovieService } from 'src/app/services/movie.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-listmovies',
  templateUrl: './listmovies.component.html',
  styleUrls: ['./listmovies.component.css']
})
export class ListmoviesComponent implements OnInit {
  searchText:any;
  movies: any []=[];
  isInWish:any;

  user={
    email:'',
    password:''
  }
  islogged : boolean =false ;
  
  userlogged =this._authService.getUserStatus();
  constructor(private _movieService:MovieService,private toastr: ToastrService,private _authService:AuthService) { 
   
  }
  

  ngOnInit(): void {
    this.getMovies();
    this.obtainLoggedUser()
  }

  getMovies(){

    this._movieService.getMovies().subscribe(data =>{
      this.movies=[];
      data.forEach((element:any) => {
        this.movies.push({
          id:element.payload.doc.id,
          ...element.payload.doc.data()
        })

      });

    });

  }
  addToWish(id:string ,num:string,title:string,year:string,genre:string,){
    
    const movie:any={
      num:num,
      title:title,
      year:year,
      genre:genre    
      
    }

    this._movieService.addToWish(id,movie);
//code to send the data to the other item 

  this._movieService.eraseMovie(id).then(()=>{

    this.toastr.success('Movie added to wishlist', 'Message',{positionClass: 'toast-bottom-right'}); 

  }).catch(error =>{
    console.log(error);
  }
    )

  }
  obtainLoggedUser(){
    this._authService.getUserStatus().subscribe(res=>{
     if(res?.email){
       this.user.email=res.email;
      
       this.islogged=(this.userlogged!=null);
     }
  
        })
        
  }

    //is id in wwish1?
    //if  yes {
    //*this._movieService.addToWish(movie).then(()=>{
         //this._movieService.eraseMovie(id)

     //* this.toastr.success('Movie added to wishlist', 'Success',{positionClass: 'toast-bottom-right'}); 
  
    //*}).catch(error =>{
     //* console.log(error);
    //*}
      //*)

}



    /*this._movieService.getToWish(id).subscribe(data=>{
      const movie : any={ 
        title : data['title'],
        year : data['year'],
        genre:data['genre']

      }
      console.log(data);

   
    })*/

//code to send the data to the other item 
/*
 
 */