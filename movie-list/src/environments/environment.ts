// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: "AIzaSyDvCwEkJBefXKXgLZJmrCxTzAe0T2rw1Y0",

    authDomain: "mymovieproj.firebaseapp.com",
  
    databaseURL: "https://mymovieproj-default-rtdb.firebaseio.com",
  
    projectId: "mymovieproj",
  
    storageBucket: "mymovieproj.appspot.com",
  
    messagingSenderId: "893993549132",
  
    appId: "1:893993549132:web:4f0e8900220df584b8a3e9",
  
    measurementId: "G-4MSNXQFERB"
  },
  production: false
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
