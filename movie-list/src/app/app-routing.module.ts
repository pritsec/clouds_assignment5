import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { ListmoviesComponent } from './components/listmovies/listmovies.component';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { LoginComponent } from './login/login.component';

const routes:Routes=[
  {path:'',redirectTo:'listmovies',pathMatch:'full'},
  {path:'wishlist',component:WishlistComponent},
  {path:'login',component:LoginComponent},

  {path:'listmovies',component:ListmoviesComponent},
  {path:'**',redirectTo:'listmovies',pathMatch:'full'}  // imórtant its at the end

];

@NgModule({
  declarations: [],
  imports: [CommonModule,RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
