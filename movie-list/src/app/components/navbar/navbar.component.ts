import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user={
    email:'',
    password:''
  }
  islogged:boolean=false;
  userlogged=this._authService.getUserStatus()
  constructor(private _authService:AuthService) { }

  ngOnInit(): void {
    this.obtainLoggedUser();
  }

  obtainLoggedUser(){
     this._authService.getUserStatus().subscribe(res=> {
      if(res?.email){
        this.user.email=res.email;
       
        this.islogged=(this.userlogged!=null);
      }
    })
         
  }

  logOut(){
    this._authService.logout()
    //this._loginService.islogged=0;
   
  }

}
