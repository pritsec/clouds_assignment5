import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  movies:any[]=[]

  constructor(private _movieService:MovieService,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getMovies();

  }
  getMovies(){

    this._movieService.getWish().subscribe(data =>{
      this.movies=[];
      data.forEach((element:any) => {
        this.movies.push({
          id:element.payload.doc.id,
          ...element.payload.doc.data()
        })

      });

    });

  }

 
 
 
 addToMovie(id:string,num:string,title:string,year:string,genre:string){

  const movie:any={
    num:num,
    title:title,
    year:year,
    genre:genre,
   
  }
  this._movieService.addToMovies(id,movie);
//code to send the data to the other item 

  this._movieService.eraseWish(id).then(()=>{

    this.toastr.error('Movie erased from Movielist', 'Message',{positionClass: 'toast-bottom-right'}); 

  }).catch(error =>{
    console.log(error);
  }
    )

  }
}
