import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

user={
  email:'',
  password:''
}
islogged : boolean =false ;

userlogged =this._authService.getUserStatus();
  constructor(private _authService:AuthService,private router:Router) { }

  ngOnInit(): void {
  
    this.obtainLoggedUser();

  }

 
  loginNow(){
    const {email,password}=this.user;
    this._authService.login(email,password);

}
obtainLoggedUser(){
  this._authService.getUserStatus().subscribe(res=>{
   if(res?.email){
     this.user.email=res.email;
    
     this.islogged=(this.userlogged!=null);
   }

      })
      
}

//probably not used
obtainLoggedState(){
  this._authService.getUserStatus().subscribe(
    data=> {
     // this.userlogged=data;
      this.islogged=(this.userlogged!=null);
    }

  )
    
}
}
